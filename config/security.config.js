const auth = require("@dyma/auth");

exports.ensureAuthenticatedOnSocketHandshake = async (request, success) => {
  auth(request);
  if (request.user) {
    success(null, true);
  } else {
    success(400, false);
  }
};
