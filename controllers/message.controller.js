const { createMessage } = require("../queries/message.queries");

exports.createMessageController = async ({ text, roomId, ios, socket }) => {
  try {
    const { _id, firstName } = socket.request.user;
    const message = await createMessage({
      data: text,
      room: roomId,
      author: _id,
      authorName: firstName,
    });
    ios.to(`/${roomId}`).emit("message", message);
  } catch (e) {
    throw e;
  }
};
