const { Room } = require("./models");
const mongoose = require("mongoose");

mongoose
  .connect(
    "mongodb+srv://admin:250789@cluster0-2btph.gcp.mongodb.net/dyma-chat?retryWrites=true&w=majority",
    {
      useCreateIndex: true,
      useNewUrlParser: true,
    }
  )
  .then(() => {
    console.log("connexion ok !");
    const room1 = new Room({
      index: 0,
      title: "Général",
    });
    const room2 = new Room({
      index: 1,
      title: "Hors sujet",
    });
    Promise.all([room1.save(), room2.save()]).then(() => {
      console.log("rooms are created");
    });
  })
  .catch((err) => {
    console.log(err);
  });
